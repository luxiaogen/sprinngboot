## 基础配置
![img_1.png](img_1.png)

![img.png](img.png)
- 推荐使用yml


## 配置文件的优先级
1. 配置文件间的加载优先级
   - properties(最高)
   - yml
   - yaml(最低)
2. 不同配置文件中相同配置按照架子啊优先级相互覆盖，不同配置文件中不同配置全部保留
3. 常用.yml文件格式

## 教你一招：自动提示功能小时解决方案
![img_2.png](img_2.png)


## yaml
- YAML(Y