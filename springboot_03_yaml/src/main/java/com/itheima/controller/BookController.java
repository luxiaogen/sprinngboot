package com.itheima.controller;

import com.itheima.MyDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陆小根
 * date: 2022/05/05
 * Description:
 */
// Rest模式
@RestController
@RequestMapping("/books")
public class BookController {

  // 读取yaml税局中的单一数据
  @Value("${country}")
  private String country;

  // 两级
  @Value("${user.name}")
  private String name;

  @Value("${likes[1]}")
  private String like;

  @Value("${users[1].age}")
  private String age;

  @Value("${server.port}")
  private String port;

  @Value("${tempDir}") // 使用自动装配将所有数据封装到一个对象中
  private String tempDir;

  // 使用
  @Autowired
  private Environment env;

  @Autowired
  private MyDataSource myDataSource;

  @GetMapping
  public String getById() {
    System.out.println("springboot is running yml ...");
    System.out.println("country ===> " + country);
    System.out.println("name ===> " + name);
    System.out.println("like ====> " + like);
    System.out.println("age ====> " + age);
    System.out.println("port ===> " + port);
    System.out.println("tempDir ===> " + tempDir);
    System.out.println("-------------------");
    System.out.println(env.getProperty("server.port"));
    System.out.println(myDataSource);
    return "springboot is running yaml ...";
  }

}
