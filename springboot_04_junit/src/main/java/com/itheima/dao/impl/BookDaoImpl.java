package com.itheima.dao.impl;

import com.itheima.dao.BookDao;
import org.springframework.stereotype.Repository;

/**
 * @author 陆小根
 * date: 2022/05/06
 * Description:
 */
@Repository
public class BookDaoImpl implements BookDao {
  @Override
  public void save() {
    System.out.println("book dao is running ...");
  }
}
