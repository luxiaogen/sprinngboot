## SpringBoot整合MyBatis步骤
①：创建新模块，选择Spring初始化，并配置相关基础信息
②：选择当前模块需要使用的技术集(MyBatis、Mysql)
③：设置数据源参数
```yaml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/ssm_db
    username: root
    password: root
```
④：定义数据层接口与映射配置
```java
@Mapper
public interface BookDao {

  @Select("select * from tbl_book where id = #{id}")
  Book getById(@Param("id")Integer id);

}
```
⑤：测试中注入dao接口，测试功能
```java
@SpringBootTest
class Springboot05MybatisApplicationTests {

  @Autowired
  private BookDao bookDao;

  @Test
  void contextLoads() {
    Book book = bookDao.getById(1);
    System.out.println("book = " + book);
  }

}
```

### 小结
```markdown
1.MySQL 8.X驱动强制要求设置时区
- 修改url，添加serverTimezone设定
- 修改MySQL数据库配置(略)
2.驱动类过时，提醒更换为com.mysql.cj.jdbc.Driver
```


  
