package com.itheima.dao;

import com.itheima.domain.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author 陆小根
 * date: 2022/05/06
 * Description:
 */
@Mapper
public interface BookDao {

  @Select("select * from tbl_book where id = #{id}")
  Book getById(@Param("id")Integer id);

}
