## SpringBoot整合MyBatis-Plus步骤
- MyBatis-Plus与MyBatis区别
    - 导入坐标不同
    - 数据层实现简化
    

①：手动添加SpringBoot整合MyBatis-Plus的坐标，可以通过mvnrepository获取
```xml
<!--mybatis-plus-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.1</version>
</dependency>
```
> 注意事项：由于SpringBoot中未收录MyBatis-Plus的坐标版本，需要指定对应的version

②：定义数据层接口与映射配置，继承`BaseMapper`
```java
@Mapper
public interface BookDao extends BaseMapper<Book> {
}
```
③：与Mybatis整合一样