## 整合Druid
- 指定数据源类型
```yml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/ssm_db?serverTimezone=UTC
    username: root    
    password: root
    type: com.alibaba.druid.pool.DruidDataSource
```
- 导入Druid对应的starer
```xml
<!--druid数据源-->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>1.2.9</version>
</dependency>
```
- 变更Druid的配置方式
```yaml
# druid数据源专用配法  也可以采用上面的配法
spring:
  datasource:
    druid:
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/ssm_db?serverTimezone=UTC
      username: root
      password: root
```
### 总结
1.整合Druid需要导入Druid对应的stater
2.根据Druid提供的配置方式进行配置
3.整合任意第三方技术
  - 导入对应的starter
  - 配置对应的或采用默认配置