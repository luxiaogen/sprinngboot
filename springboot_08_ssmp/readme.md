# 案例实现方案分析
- 实体类开发——使用Lombok快速制作实体类
- Dao开发——整合MybatisPlus，制作数据层测试类
- Service开发——基于MyBatisPlus进行增量开发，制作业务层测试类
- Controller开发——基于Restful开发，使用Postman测试接口功能
- Controller开发——前后端开发协议制作
- 页面开发——基于VUE+ElementUI制作，前后端联调，页面数据处理，页面消息处理
  - 列表、新增、修改、删除、分页、查询
- 项目异常处理
- 按条件查询——页面功能调整、Controller修正功能、Service修正功能

## 数据层  mybatis-plus  druid
1. 手动导入starter坐标(2个)
2. 配置数据源与MyBatisPlus对应的配置
3. 开发Dao接口(继承BaseMapper)
4. 制作测试类测试Dao功能是否有效
### 数据层开发-日志功能
- 为方便调试可以开启MyBatisPlus的日志
```yaml
mybatis-plus:
  # 配置日志
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```
### 数据层开发-分页功能
- 分页操作需要设定分页对象`IPage`
```markdown
public void testGetPage() {
    // 使用myabtis-plus的分页时必须使用mp提供的拦截器
    IPage<Book> page = new Page<>(1,5);
    IPage<Book> bookIPage = bookDao.selectPage(page, null);
    System.out.println(bookIPage.getCurrent());
    System.out.println(bookIPage.getSize());
    System.out.println(bookIPage.getTotal());
    System.out.println(bookIPage.getPages());
    System.out.println(bookIPage.getRecords());
  }
```
- `IPage对象中封装了分页操作中的所有数据`
  - 数据 getRecords
  - 当前页码值 getCurrent
  - 每页数据总量 getSize
  - 最大页码值 getTotal
  - 数据总量  getTotal
- 分析操作是MyBatisPlus的常规操作基础上增强得到，内部是动态的拼写SQL语句，因此需要增强对应的功能，使用MyBatisPlus拦截器实现
```java
@Configuration
public class MPConfig {
  @Bean
  public MybatisPlusInterceptor mybatisPlusInterceptor() {
    // ----> 实现分页
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor()); // 分页拦截器  具体的拦截器
    return interceptor;
  }
}
```
### 数据层开发-条件查询功能
- 使用QueryWrapper对象封装查询条件，推荐使用LambdaQueryWrapper对象，所有查询操作封装成方法调用
```markdown
  @Test
  public void testGetBy() {
    // 按条件查询
    QueryWrapper<Book> qw = new QueryWrapper<>();
    qw.like("name", "Spring");
    bookDao.selectList(qw);
  }

  @Test
  public void testGetBy2() {
    // 按条件查询
    String name = "Spring";
    LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
//    if (name != null) lqw.like(Book::getName, name);
    lqw.like(name!=null,Book::getName, name); // condition false --> 不连 true ---> 连
    bookDao.selectList(lqw);
  }
```
- 支持动态拼写查询条件
![img.png](img.png)

## 业务层开发
- Service层接口定义与数据与数据层接口定义具有较大区别，不要混用
  - selectByUserNameAndPassword(String username, String password)
  - login(String username, String password)
- 接口定义
```java
public interface BookService {

  Boolean save(Book book);

  Boolean update(Book book);

  Boolean delete(Integer id);

  Book getById(Integer id);

  List<Book> getAll();

  IPage<Book> getPage(int current, int pageSize);

}
```
- 方法的实现
```java
@Service
public class BookServiceImpl implements BookService {

  private final BookDao bookDao;

  @Autowired
  public BookServiceImpl(BookDao bookDao) {
    this.bookDao = bookDao;
  }

  @Override
  public Boolean save(Book book) {
    return bookDao.insert(book) > 0;
  }

  @Override
  public Boolean update(Book book) {
    return bookDao.updateById(book) > 0;
  }

  @Override
  public Boolean delete(Integer id) {
    return bookDao.deleteById(id) > 0;
  }

  @Override
  public Book getById(Integer id) {
    return bookDao.selectById(id);
  }

  @Override
  public List<Book> getAll() {
    return bookDao.selectList(null);
  }

  @Override
  public IPage<Book> getPage(int current, int pageSize) {
    IPage<Book> page = new Page<>(current, pageSize);
    bookDao.selectPage(page, null);
    return page;
  }
}
```
- 快速开发方案
  - 使用MyBatisPlus提供有业务层通过接口(`IService<T>`)与业务层通过实现类(`ServiceImpl><M,T>`)
  - 在通用类基础上做功能重载或功能追加
  - 注意重载时不要覆盖原始操作，避免原始体用的功能丢失

![img_1.png](img_1.png)
- 接口定义
![img_2.png](img_2.png)
- 实现列定义
```java
@Service
public class BookServiceImpl extends ServiceImpl<BookDao, Book> implements IBookService  {
}
```
- 实现类追加功能
![img_3.png](img_3.png)

```markdown
# 总结
1.使用通过接口(`IService<T>`快速开发Service)
2.使用通用实现类(ServiceImpl<M,T>)快速开发ServiceImpl
3.可以在通用接口基础上做功能重载或功能追加
4.注意重载时不要覆盖原始操作，避免原始提供的功能丢失
```

## 表现层开发
- 基于Restful进行表现层接口开发
- 使用Postman测试表示接口功能

```markdown
1. 基于Restful制作表现层接口
    - 新增：POST
    - 删除：DELETE
    - 修改：PUT
    - 查询：GET
2. 接收参数
    - 实体数据：@RequestBody 请求体中去数据
    - 路径变量：@PathVariable
```

### 表现层消息一致性处理
![img_4.png](img_4.png)
统一格式
![img_5.png](img_5.png)
- 设计表现层返回结果的模型类，用于后端与前端进行格式统一，也称为**前后端数据协议**

```java
@Data
public class R {
  private Boolean flag;
  private Object data;
  public R() {
  }
  public R(Boolean flag) {
    this.flag = flag;
  }
  public R(Boolean flag, Object data) {
    this.flag = flag;
    this.data = data;
  }
}
```
- 表现层接口统一返回值类型结果
![img_6.png](img_6.png)

```markdown
# 总结
1. 设计统一的返回值结果类型便于前端开发读取数据
2. 返回值结果类型可以根据需求自行设定，没有固定格式
3. 返回值结果模型类用于后端与前端进行数据格式统一，也称为**前后端数据协议(json)**
```
### 前后端协议联调
- 前后端分离结构设计中页面归属前端服务器
- 单体工程中页面放置在resources目录下的static目录中(建议执行**clean**)
- 前端发送异步请求，调用后端接口
```markdown
1. 单体项目中页面设置在resources/static目录下
2. created钩子函数用于初始化页面时发起调用
3. 页面使用axios发送异步请求获取数据后确认前后端是否联通

## 删除功能
1. 请求方式使用Delete调用后台对应操作
2. 删除操作需要传递当前行数数据对应的id到后台
3. 删除操作结束后动态刷新页面加载数据
4. 根据操作结果不同，显示对应的提示信息
5. 删除操作前弹出提示框避免误操作

## 修改功能
1.请求方式使用PUT调用后台对应操作
2.修改操作结束后动态刷新页面加载数据(同新增)
3.根据操作结果不同，显示对应的提示信息(同新增)
```
### 业务消息一致性处理
![img_7.png](img_7.png)
- 对异常进行统一处理，出现异常后，返回指定信息
```java
//@ControllerAdvice
@RestControllerAdvice
public class ProjectExceptionAdvice {

  // 拦截所有的异常信息
  @ExceptionHandler
  public R doException(Exception ex) {
    // 记录日志
    // 通知运维
    // 通知开发
    ex.printStackTrace();
    return new R("服务器故障，请稍后再试!");
  }
}
```
- 修改表现层返回结果的模型，封装出现异常后对应的信息
  - flag：false
  - Data：null
  - 消息(msg):要显示信息系
```java
@Data
public class R {
  private Boolean flag;
  private Object data;
  private String msg;
}
```
- 页面消息处理，没有传递消息加载默认消息，传递消息后加载指定消息
- 可以在表现层Controller中进行消息统一处理
```markdown
@PostMapping
public R save(@RequestBody Book book) {
  boolean flag = bookService.save(book);
  return new R(flag, flag ? "添加成功^_^":"添加失败-_-!");
}
```
```markdown
handleAdd () {
    axios.post(`/books`, this.formData).then(resp => {
        // 判断当前操作是否成功
        if (resp.data.flag) { // 添加成功
            // 关闭弹层
            this.dialogFormVisible = false
            this.$message.success(resp.data.msg)
        } else { // 失败
            this.$message.error(resp.data.msg)
        }
    }).finally(()=>{
        // 重新加载数据
        this.getAll()
    })
}
```
  - 目的：国际化
```markdown
# 小结：
1. 使用@RestControllerAdvice定义SpringMVC异常器用来处理异常的
2. 异常处理器必须被扫描加载，否则无法生效
3. 表示层返回结果的模型类中添加消息属性用来传递消息到页面
```

### 分页功能
- 页面使用el分页组件添加分页功能
```html
<!--分页组件-->
<div class="pagination-container">
    <el-pagination
            class="pagiantion"
            @current-change="handleCurrentChange"
            :current-page="pagination.currentPage"
            :page-size="pagination.pageSize"
            layout="total, prev, pager, next, jumper"
            :total="pagination.total">
    </el-pagination>
</div>
```
- 定义分页组件需要使用的数据并将数据把绑定到分页组件
```vue
data:{
  pagination: {//分页相关模型数据
    currentPage: 1,//当前页码
    pageSize:10,//每页显示的记录数22222
    total:0//总记录数
  }
}
```
- 替换查询全部功能为分页功能
- 加载分页数据
- 分页页面值切换
```vue
//切换页码
            handleCurrentChange(currentPage) {
                // 修改页面值为选中选课的页码值
                this.pagination.currentPage = currentPage
                // 执行查询
                this.getAll()
            },
```
```markdown
# 总结
1. 使用el分页组件
2. 定义分页组件绑定的数据模型
3. 异步调用获取分页数据
4. 分页数据页面回显
```
- 查询条件数据封装
  - 单独封装
  - 与分页操作混合封装
```vue
data:{
  pagination: {//分页相关模型数据
    currentPage: 1,//当前页码
    pageSize:10,//每页显示的记录数22222
    total:0,//总记录数
    type: "", // 图书类名
    name: "", // 图书名称
    description: "" // 图书描述    
  }
}
```
- 组织数据称为get请求发送的数据
![img_8.png](img_8.png)
- 条件参数组织可以通过条件判断书写的更简洁
- Controller接收参数
![img_9.png](img_9.png)


## 总结
```markdown
1. pom.xml 
配置起步依赖
2. application.yml
设置数据源、端口、框架技术相关配置
3.dao
继承BaseMapper、设置@Mapper
4.dao测试类
5.service
调用数据层接口或MyBatis-Plus听的接口快速开发
6.service测试类
7.controller
基于Restful开发，使用Postman测试跑通功能
8.页面
放置在resources目录下的static目录下
```
