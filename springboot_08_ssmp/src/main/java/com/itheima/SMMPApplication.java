package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SMMPApplication {

  public static void main(String[] args) {
    SpringApplication.run(SMMPApplication.class, args);
  }

}
