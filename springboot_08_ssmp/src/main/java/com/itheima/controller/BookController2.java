package com.itheima.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itheima.domain.Book;
import com.itheima.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
//@RestController
@RequestMapping("/books")
public class BookController2 {

  private final IBookService bookService;

  @Autowired
  public BookController2(IBookService bookService) {
    this.bookService = bookService;
  }

  /**
   * 获取所有图书信息
   * @return
   */
  @GetMapping
  public List<Book> getAll() {
    return bookService.list();
  }

  /**
   * 新增图书
   * @param book
   * @return
   */
  @PostMapping
  public Boolean save(@RequestBody Book book) {
    return bookService.save(book);
  }

  /**
   * 修改图书信息
   * @param book
   * @return
   */
  @PutMapping
  public Boolean update(@RequestBody Book book) {
    return bookService.updateById(book);
  }

  /**
   * 根据图书编号删除图书
   * @param id
   * @return
   */
  @DeleteMapping("{id}")
  public Boolean delete(@PathVariable("id") Integer id) {
    return bookService.removeById(id);
  }

  /**
   * 根据图书编号查询图书信息
   * @param id
   */
  @GetMapping("{id}")
  public Book getById(@PathVariable("id") Integer id) {
    return bookService.getById(id);
  }


  /**
   * 分页查询功能
   * @param currentPage
   * @param pageSize
   * @return
   */
  @GetMapping("{currentPage}/{pageSize}")
  public IPage<Book> getPage(@PathVariable("currentPage") int currentPage, @PathVariable("pageSize") int pageSize) {
    return bookService.getPage(currentPage, pageSize);
  }
}
