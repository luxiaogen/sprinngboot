package com.itheima.controller.utils;

import lombok.Data;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@Data
public class R {

  private Boolean flag;
  private Object data;
  private String msg;

  public R() {
  }

  public R(Boolean flag) {
    this.flag = flag;
  }

  public R(Boolean flag, Object data) {
    this.flag = flag;
    this.data = data;
  }

  public R(Boolean flag, String msg) {
    this.flag = flag;
    this.msg = msg;
  }

  public R(String msg) {
    this.flag = false;
    this.msg = msg;
  }
}
