package com.itheima.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@Mapper
public interface BookDao extends BaseMapper<Book> {
}
