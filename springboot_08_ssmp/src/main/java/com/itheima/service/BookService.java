package com.itheima.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itheima.domain.Book;

import java.util.List;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */

public interface BookService {

  Boolean save(Book book);

  Boolean update(Book book);

  Boolean delete(Integer id);

  Book getById(Integer id);

  List<Book> getAll();

  IPage<Book> getPage(int current, int pageSize);

}
