package com.itheima.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.domain.Book;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */

public interface IBookService extends IService<Book> {


  /**
   * 分页查询
   * @param currentPage
   * @param pageSize
   * @return
   */
  IPage<Book> getPage(int currentPage, int pageSize);

  /**
   * 分页查询+条件查询
   * @param currentPage
   * @param pageSize
   * @param book
   * @return
   */
  IPage<Book> getPage(int currentPage, int pageSize, Book book);
}
