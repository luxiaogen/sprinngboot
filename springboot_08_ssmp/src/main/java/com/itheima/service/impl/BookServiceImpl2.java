package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.dao.BookDao;
import com.itheima.domain.Book;
import com.itheima.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@Service
public class BookServiceImpl2 implements BookService {

  private final BookDao bookDao;

  @Autowired
  public BookServiceImpl2(BookDao bookDao) {
    this.bookDao = bookDao;
  }

  @Override
  public Boolean save(Book book) {
    return bookDao.insert(book) > 0;
  }

  @Override
  public Boolean update(Book book) {
    return bookDao.updateById(book) > 0;
  }

  @Override
  public Boolean delete(Integer id) {
    return bookDao.deleteById(id) > 0;
  }

  @Override
  public Book getById(Integer id) {
    return bookDao.selectById(id);
  }

  @Override
  public List<Book> getAll() {
    return bookDao.selectList(null);
  }

  @Override
  public IPage<Book> getPage(int current, int pageSize) {
    IPage<Book> page = new Page<>(current, pageSize);
    bookDao.selectPage(page, null);
    return page;
  }
}
