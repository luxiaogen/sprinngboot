package com.itheima.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@SpringBootTest
public class BookDaoTestCase {

  @Autowired
  private BookDao bookDao;

  @Test
  public void testSelectyId() {
    Book book = bookDao.selectById(1);
    System.out.println(book);
  }

  @Test
  public void testSave() {
    Book book = new Book();
    book.setType("测试数据123");
    book.setName("测试数据123");
    book.setDescription("测试数据123");
    bookDao.insert(book);
  }

  @Test
  public void testUpdate() {
    Book book = new Book();
    book.setId(16);
    book.setType("测试数据abc");
    book.setName("测试数据123");
    book.setDescription("测试数据123");
    bookDao.updateById(book);
  }

  @Test
  public void testDelete() {
    bookDao.deleteById(16);
  }

  @Test
  public void testGetAll() {
    bookDao.selectList(null);
  }

  @Test
  public void testGetPage() {
    // 使用myabtis-plus的分页时必须使用mp提供的拦截器
    IPage<Book> page = new Page<>(1,5);
    IPage<Book> bookIPage = bookDao.selectPage(page, null);
    System.out.println(bookIPage.getCurrent());
    System.out.println(bookIPage.getSize());
    System.out.println(bookIPage.getTotal());
    System.out.println(bookIPage.getPages());
    System.out.println(bookIPage.getRecords());
  }

  @Test
  public void testGetBy() {
    // 按条件查询
    QueryWrapper<Book> qw = new QueryWrapper<>();
    qw.like("name", "Spring");
    bookDao.selectList(qw);
  }

  @Test
  public void testGetBy2() {
    // 按条件查询
    String name = "Spring";
    LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
//    if (name != null) lqw.like(Book::getName, name);
    lqw.like(name!=null,Book::getName, name); // condition false --> 不连 true ---> 连
    bookDao.selectList(lqw);
  }

}
