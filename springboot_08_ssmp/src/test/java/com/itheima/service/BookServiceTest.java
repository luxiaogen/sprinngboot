package com.itheima.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@SpringBootTest
public class BookServiceTest {

  @Autowired
  private IBookService bookService;

  @Test
  void testGetById() {
    Book book = bookService.getById(1);
    System.out.println(book);
  }

  @Test
  public void testSave() {
    Book book = new Book();
    book.setType("测试数据123");
    book.setName("测试数据123");
    book.setDescription("测试数据123");
    bookService.save(book);
  }

  @Test
  public void testUpdate() {
    Book book = new Book();
    book.setId(17);
    book.setType("测试数据abc");
    book.setName("测试数据123");
    book.setDescription("测试数据123");
    bookService.updateById(book);
  }

  @Test
  public void testDelete() {
    bookService.removeById(18);
  }

  @Test
  public void testGetAll() {
    bookService.list();
  }

  @Test
  public void testGetPage() {
    // 使用myabtis-plus的分页时必须使用mp提供的拦截器
    IPage<Book> page = new Page<Book>(2,5);
    IPage<Book> bookIPage = bookService.page(page);
    System.out.println(bookIPage.getCurrent());
    System.out.println(bookIPage.getSize());
    System.out.println(bookIPage.getTotal());
    System.out.println(bookIPage.getPages());
    System.out.println(bookIPage.getRecords());
  }

}
