# 热部署
## 启动热部署
- 开启开发者工具
```xml

```
- 激活热部署：Ctrl+F9
- 关于热部署
  - 重启(Restart)：自定义开发代码，包括类，页面，配置文件等，加载位置restart类加载器
  - 重载(Reload)：jar包，加载位置base类加载器
```markdown
# 小结
1.开启开发者工具后启用热部署
2.使用构建项目操作启动热部署(Ctrl+F9)
3.热部署仅仅加载当前开发者自定义开发的资源，不加载jar资源
```

## 自动启动热部署
![img_1.png](img_1.png)
![img.png](img.png)
- 激活方式：IDEA失去焦点5秒后启动热部署


## 热部署范围配置
- 默认不触发重启的目录列表
  - /META-INF/maven
  - /META-INF/resources
  - /resources
  - /static
  - /public 
  - /templates
- 自定义不参与重启排除项
```yaml
spring:
  # 修改热部署配置
  devtools:
    restart:
      # 设置不参与热部署的文件或文件夹
      exclude: static/**,public/**,config/application.yml
```

## 关闭热部署功能
- 配置文件
```yaml
spring:
  # 修改热部署配置
  devtools:
    restart:
      # 设置不参与热部署的文件或文件夹
      exclude: static/**,public/**,config/application.yml
      # 关闭热部署
      enabled: false
```
- 设置优先级属性禁止热部署
```java
@SpringBootApplication
public class HotDeployApplication {

  public static void main(String[] args) {
    // 可以保证覆盖配置文件里的数据
    System.setProperty("spring.devtools.restart.enabled", "false");
    SpringApplication.run(HotDeployApplication.class, args);
  }

}
```
