package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotDeployApplication {

  public static void main(String[] args) {
    // 可以保证覆盖配置文件里的数据
    System.setProperty("spring.devtools.restart.enabled", "false");
    SpringApplication.run(HotDeployApplication.class, args);
  }

}
