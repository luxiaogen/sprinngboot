package com.itheima.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 * 定义拦截器  ---> 实现分页
 */
@Configuration
public class MPConfig {

  @Bean
  public MybatisPlusInterceptor mybatisPlusInterceptor() {
    // ----> 实现分页
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor()); // 分页拦截器  具体的拦截器
    return interceptor;
  }

}
