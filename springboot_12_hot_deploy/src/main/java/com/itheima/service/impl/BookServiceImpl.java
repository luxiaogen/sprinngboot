package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dao.BookDao;
import com.itheima.domain.Book;
import com.itheima.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 陆小根
 * date: 2022/05/07
 * Description:
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookDao, Book> implements IBookService  {

  @Autowired
  private BookDao bookDao;

  @Override
  public IPage<Book> getPage(int currentPage, int pageSize) {
    IPage<Book> page = new Page<>(currentPage, pageSize);
    return bookDao.selectPage(page, null);
  }

  @Override
  public IPage<Book> getPage(int currentPage, int pageSize, Book book) {
    LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
    lqw.like(StringUtils.isNotBlank(book.getType()), Book::getType, book.getType());
    lqw.like(StringUtils.isNotBlank(book.getName()), Book::getName, book.getName());
    lqw.like(StringUtils.isNotBlank(book.getDescription()), Book::getDescription, book.getDescription());
    IPage<Book> page = new Page<>(currentPage, pageSize);
    return bookDao.selectPage(page, lqw);
  }
}
