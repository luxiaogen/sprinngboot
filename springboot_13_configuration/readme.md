## 第三方bean属性绑定
### @ConfigurationProperties
- 使用`@ConfigurationProperties`为第三方bean绑定属性
```
@Bean
@ConfigurationProperties(prefix = "datasource")
public DruidDataSource dataSource() {
    DruidDataSource ds = new DruidDataSource();
    return ds;
}
```
```yaml
datasource:
  driverClassName: com.mysql.cj.jdbc.Driver
```
- 解除使用`ConfigurationProperties`注释警告

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
</dependency>
```
### @EnableConfigurationProperties
- @EnableConfigurationProperties注解可以将使用@ConfigurationProperties注解对应的类加入Spring容器
```java
@SpringBootApplication
@EnableConfigurationProperties(ServerConfig.class)
public class Springboot13ConfigurationApplication {
  @Bean
  @ConfigurationProperties(prefix = "datasource")
  public DruidDataSource dataSource() {
    DruidDataSource ds = new DruidDataSource();
    return ds;
  }
}
```
```java
//@Component // 受Spring管控
@Data
@ConfigurationProperties(prefix = "servers")
public class ServerConfig {
}
```
> 注意事项：`@EnableConfigProperties`后面的bean与`@Component`不能同时使用

## 宽松绑定
- `@ConfigurationProperties`绑定属性支持属性名宽松绑定
```java
public class ServerConfig {
  private String ipAddress;
  private int port;
  private long timeout;
}
```
![img.png](img.png)
> 注意事项：宽松绑定不支持注解`@Value`引用单个属性的方式
 ```
@Value("${servers.ip-address}")
private String msg;
```

```
@Bean
@ConfigurationProperties(prefix = "datasource")
public DruidDataSource dataSource() {
    DruidDataSource ds = new DruidDataSource();
    return ds;
}
```
> 注意事项：绑定前缀名命名规范：仅能使用**纯小写字母、数字、下划线**作为合法字符

## 常用计量单位
- SpringBoot支持JDK8提供的时间与空间计量单位
```java
//@Component // 受Spring管控
@Data
@ConfigurationProperties(prefix = "servers")
public class ServerConfig {
  private String ipAddress;
  private int port;
  private long timeout; // 毫秒
  @DurationUnit(ChronoUnit.HOURS) // 小时~
  private Duration serverTimeOut;
  @DataSizeUnit(DataUnit.MEGABYTES) // mb
  private DataSize dataSize;
}
```
![img_1.png](img_1.png)

## 数据校验
### 开启Bean数据校验
1.添加JSR303规范坐标与Hibernate校验框架对应坐标
```
<!--1.导入JSR303规范 数据校验  一套规范-->
<dependency>
    <groupId>javax.validation</groupId>
    <artifactId>validation-api</artifactId>
</dependency>

<!--使用hibernate框架听的校验器做实现类-->
<dependency>
    <groupId>org.hibernate.validator</groupId>
    <artifactId>hibernate-validator</artifactId>
</dependency>
```
2.对bean开启校功能
```java
//@Component // 受Spring管控
@Data
@ConfigurationProperties(prefix = "servers")
// 2.开启对当前bean的属性注入校验
@Validated
public class ServerConfig {
}
```
3.设置校验规则
```java
//@Component // 受Spring管控
@Data
@ConfigurationProperties(prefix = "servers")
// 2.开启对当前bean的属性注入校验
@Validated
public class ServerConfig {
  // 3.设置具体的规则
  @Max(value = 8888,message = "最大值不能超过8888")
  @Min(value = 202, message = "最小值不能低于202")
  private int port;
}
```
```markdown
# 小结
1. 启动Bean属性校验
- 导入JSR303与Hibernate校验框架坐标
- 使用`@Validated`注解启用校验功能
- 使用具体校验规则规范数据校验格式
```

## 数据类型转换
- yaml语法规则
  - 字面值表达方式
![img_2.png](img_2.png)
**注意yaml文件中对于数字的定义支持进制书写格式，如需要使用字符串请使用引号明确标注**

