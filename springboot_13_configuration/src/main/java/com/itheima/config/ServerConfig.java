package com.itheima.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * @author 陆小根
 * date: 2022/05/08
 * Description:
 */
//@Component // 受Spring管控
@Data
@ConfigurationProperties(prefix = "servers")
// 2.开启对当前bean的属性注入校验
@Validated
public class ServerConfig {

  private String ipAddress;
  // 3.设置具体的规则
  @Max(value = 8888,message = "最大值不能超过8888")
  @Min(value = 202, message = "最小值不能低于202")
  private int port;
  private long timeout; // 毫秒

  @DurationUnit(ChronoUnit.HOURS) // 小时~
  private Duration serverTimeOut;

//  @DataSizeUnit(DataUnit.MEGABYTES) // mb
  private DataSize dataSize;

}
