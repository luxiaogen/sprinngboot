# 测试
## 加载测试专用属性(临时属性)
command+F12查看该类所有的方法
- 在启动测试环境时可以通过properties参数设置测试环境专用的属性
```java
//properties属性可以为当前测试用例添加临时的属性配置
@SpringBootTest(properties = {"test.prop=testValue1"})
public class PropertiesAndArgsTest {
  @Value("${test.prop}")
  private String msg;
  @Test
  void testProperties() {
    System.out.println(msg);
  }
}
```
- 优势：比多环境开发中的测试环境影响范围更小，仅对当前测试类有效
- 在启动测试环境时可以通过args参数设置测试环境专用的传入参数
```java
//args属性可以为当前测试用例添加临时的命令行数
@SpringBootTest(args = {"--test.prop=testValue2"})
public class PropertiesAndArgsTest {
  @Value("${test.prop}")
  private String msg;
  @Test
  void testProperties() {
    System.out.println(msg);
  }
}
```
> 小结：加载测试临时属性应用小于小范围测试环境
- 使用`@Import`注解加载当前测试类专用的配置
```java
@SpringBootTest
@Import({MsgConfig.class}) // 追加了一组配置
public class ConfigurationTest {
  @Autowired
  private String msg;
  @Test
  void testConfiguration() {
    System.out.println(msg);
  }
}
```
## web环境测试
- 模拟端口
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebTest {
  @Test
  void test() {
  }
}
```
![img.png](img.png)
- 虚拟请求测试
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {
  @Test
  // 注入虚拟MVC调用对象
  void testWeb(@Autowired MockMvc mockMvc) throws Exception {
    // 创建虚拟七扭去，当前访问/books
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    // 执行请求
    ResultActions action = mockMvc.perform(builder);
  }
}
```
- 虚拟请求状态匹配
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {
  @Test
  void testStatus(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    // 设定预期值 与真实值进行比较，成功测试通过，失败测试不通过
    // 定义本次调用的预期值
    StatusResultMatchers status = MockMvcResultMatchers.status();
    // 预计本次调用时成功的：状态200
    ResultMatcher ok = status.isOk();
    // 添加预计值到本次调用过程中进行匹配
    action.andExpect(ok);
  }
}
```
- 虚拟请求体匹配
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {
  // 虚拟请求体匹配
  @Test
  void testBody(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    // 匹配执行结果(是否预期值)
    // 定义执行结果匹配器
    ContentResultMatchers content = MockMvcResultMatchers.content();
    // 定义预期执行结果
    ResultMatcher result = content.string("SpringBoot");
    // 使用本次真实执行结果与预期结果进行比对
    action.andExpect(result);
  }
}
```
- 虚拟请求体(json)匹配
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {
  // 虚拟请求体(json)匹配
  @Test
  void testJSon(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    // 匹配执行结果(是否预期值)
    // 定义执行结果匹配器
    ContentResultMatchers content = MockMvcResultMatchers.content();
    // 定义预期执行结果
    ResultMatcher result = content.json("{\n" +
            "  \"id\": 1,\n" +
            "  \"name\": \"springboot\",\n" +
            "  \"type\": \"springboot\",\n" +
            "  \"description\": \"springboot\"\n" +
            "}");
    action.andExpect(result);
  }
}
```
- 虚拟请求头匹配
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {
  // 虚拟请求体(json)匹配
  // 虚拟请求头匹配
  @Test
  void testContentType(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    HeaderResultMatchers header = MockMvcResultMatchers.header();
    ResultMatcher resultMatcher = header.string("Content-Type", "application/json");
    action.andExpect(resultMatcher);
  }
}
```
## 数据层测试事务回滚
- 为测试用例添加事务，SpringBoot会对测试用例对应的事务提交操作进行回滚
```java
@SpringBootTest
@Transactional
public class DaoTest {
  @Autowired
  private BookService bookService;
}
```
- 如果想在测试用例中提交事务，可以通过@Rollback注解设置
```java
@SpringBootTest
@Transactional
@Rollback(false)
public class DaoTest {
  @Autowired
  private BookService bookService;
}
```
## 测试用例数据设定
- 测试用例数据通常采用随机值进行测试，使用SpringBoot提供的随机数为其赋值
```yaml
testcase:
  book:
    id: ${random.int} # 随机整数
    id2: ${random.int(10)} # 10以内随机数
    type: ${random.int(10,20)} # 10到20随机数
    uuid: ${random.uuid} # 随机uuid
    name: ${random.value} # 随机字符串，MD5字符串，32位
    publishTime: ${random.long} # 随机整数(long)范围
```
- `${random.int}`表示随机整数
- `${random.int(10)}`表示10以内的随机数
- `${random.int(10,20)}`表示10到20的随机数
- 其中()可以是任意字符,例如[],!!均可



