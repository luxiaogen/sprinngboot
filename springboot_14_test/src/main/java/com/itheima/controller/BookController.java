package com.itheima.controller;

import com.itheima.domain.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */

@RestController
@RequestMapping("/books")
public class BookController {

//  @GetMapping
//  public String getById() {
//    System.out.println("getById is running...");
//    return "SpringBoot";
//  }

  @GetMapping
  public Book getById() {
    System.out.println("getById is running...");
    Book book = new Book();
    book.setId(1);
    book.setName("springboot");
    book.setType("springboot");
    book.setDescription("springboot");
    return book;
  }

}
