package com.itheima.service;

import com.itheima.domain.Book;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */

public interface BookService {

  boolean save(Book book);

}
