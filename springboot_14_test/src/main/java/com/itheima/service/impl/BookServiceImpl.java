package com.itheima.service.impl;

import com.itheima.dao.BookDao;
import com.itheima.domain.Book;
import com.itheima.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */
@Service
public class BookServiceImpl implements BookService {

  private final BookDao bookDao;

  @Autowired
  public BookServiceImpl(BookDao bookDao) {
    this.bookDao = bookDao;
  }

  @Override
  public boolean save(Book book) {
    return bookDao.insert(book) > 0;
  }
}
