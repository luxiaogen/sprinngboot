package com.itheima.testcase.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */
@Component
@Data
@ConfigurationProperties(prefix = "testcase.book")
public class BookCase {

  private Integer id;
  private Integer id2;
  private Integer type;
  private String uuid;
  private String name;
  private long publishTime;

}
