package com.itheima;

import com.itheima.config.MsgConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

/**
 * @author 陆小根
 * date: 2022/05/08
 * Description:
 */
@SpringBootTest
@Import({MsgConfig.class}) // 追加了一组配置
public class ConfigurationTest {

  @Autowired
  private String msg;

  @Test
  void testConfiguration() {
    System.out.println(msg);
  }

}
