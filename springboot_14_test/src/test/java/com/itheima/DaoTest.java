package com.itheima;

import com.itheima.domain.Book;
import com.itheima.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */

@SpringBootTest
@Transactional
@Rollback(true)
public class DaoTest {

  @Autowired
  private BookService bookService;

  @Test
  void testSave() {
    Book book = new Book();
    book.setName("springboot");
    book.setType("springboot");
    book.setDescription("springboot");
    bookService.save(book);
  }


}
