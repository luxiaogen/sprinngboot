package com.itheima;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.ContentResultMatchers;
import org.springframework.test.web.servlet.result.HeaderResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.StatusResultMatchers;

/**
 * @author 陆小根
 * date: 2022/05/09
 * Description:
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// 开启虚拟MVC调用
@AutoConfigureMockMvc
public class WebTest {

  @Test
  void testRandomPort() {
  }


  @Test
  // 注入虚拟MVC调用对象
  void testWeb(@Autowired MockMvc mockMvc) throws Exception {
    // 创建虚拟请求，当前访问/books
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    // 执行请求
    ResultActions action = mockMvc.perform(builder);
  }

  @Test
  void testStatus(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);

    // 设定预期值 与真实值进行比较，成功测试通过，失败测试不通过
    // 定义本次调用的预期值
    StatusResultMatchers status = MockMvcResultMatchers.status();
    // 预计本次调用时成功的：状态200
    ResultMatcher ok = status.isOk();
    // 添加预计值到本次调用过程中进行匹配
    action.andExpect(ok);
  }

  // 虚拟请求体匹配
  @Test
  void testBody(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    // 匹配执行结果(是否预期值)
    // 定义执行结果匹配器
    ContentResultMatchers content = MockMvcResultMatchers.content();
    // 定义预期执行结果
    ResultMatcher result = content.string("SpringBoot");
    // 使用本次真实执行结果与预期结果进行比对
    action.andExpect(result);
  }

  // 虚拟请求体(json)匹配
  @Test
  void testJSon(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    // 匹配执行结果(是否预期值)
    // 定义执行结果匹配器
    ContentResultMatchers content = MockMvcResultMatchers.content();
    // 定义预期执行结果
    ResultMatcher result = content.json("{\n" +
            "  \"id\": 1,\n" +
            "  \"name\": \"springboot\",\n" +
            "  \"type\": \"springboot\",\n" +
            "  \"description\": \"springboot\"\n" +
            "}");
    action.andExpect(result);
  }

  // 虚拟请求头匹配
  @Test
  void testContentType(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);
    HeaderResultMatchers header = MockMvcResultMatchers.header();
    ResultMatcher resultMatcher = header.string("Content-Type", "application/json");
    action.andExpect(resultMatcher);
  }

  // 一般实际开发
  @Test
  void testGetById(@Autowired MockMvc mvc) throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");
    ResultActions action = mvc.perform(builder);

    StatusResultMatchers status = MockMvcResultMatchers.status();
    ResultMatcher ok = status.isOk();
    action.andExpect(ok);


    ContentResultMatchers content = MockMvcResultMatchers.content();
    ResultMatcher result = content.json("{\n" +
            "  \"id\": 1,\n" +
            "  \"name\": \"springboot\",\n" +
            "  \"type\": \"springboot\",\n" +
            "  \"description\": \"springboot\"\n" +
            "}");
    action.andExpect(result);

    HeaderResultMatchers header = MockMvcResultMatchers.header();
    ResultMatcher resultMatcher = header.string("Content-Type", "application/json");
    action.andExpect(resultMatcher);
  }
}
