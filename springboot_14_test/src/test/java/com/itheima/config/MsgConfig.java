package com.itheima.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陆小根
 * date: 2022/05/08
 * Description:
 */
@Configuration
public class MsgConfig {

  @Bean
  public String msg() {
    return "bean msg";
  }

}
