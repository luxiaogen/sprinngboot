# SQL
## 数据源配置
- SpringBoot提供了3种内嵌的数据源对象供开发者选择
  - HikariCP
  - Tomcat提供DataSource
  - Commons DBCP
- 格式一
```yaml
spring:
  datasource:
      druid:
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://localhost:3306/ssm_db?serverTimezone=UTC
        username: root
        password: root
```
- 格式二
```yaml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/ssm_db?serverTimezone=UTC
    username: root
    password: root
    type: com.alibaba.druid.pool.DruidDataSource
```
- 通用配置无法设置具体的数据源配置信息，仅听基本的连接相关配置，如需配置，在下一级配置红设置具体设定
```yaml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/ssm_db?serverTimezone=UTC
    hikari:
      driver-class-name: com.mysql.cj.jdbc.Driver
      username: root
      password: root
```
- 内置持久化操作方案——jdbcTemplate
```java
@SpringBootTest
class Springboot15SqlApplicationTests {
  @Test
  void testJdbcTemplate(@Autowired JdbcTemplate jdbcTemplate) {
    RowMapper<Book> rm = new RowMapper<Book>(){
      @Override
      public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        Book temp = new Book();
        temp.setId(rs.getInt("id"));
        temp.setName(rs.getString("name"));
        temp.setType(rs.getString("type"));
        temp.setDescription(rs.getString("description"));
        return temp;
      }
    };
    List<Book> list = jdbcTemplate.query(sql, rm);
    System.out.println(list );
  }
}
```
- 内置持久化解决方案——jdbcTemplate
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```
- jdbcTemplate配置
```yaml
spring:
  jdbc:
    template:
      query-timeout: -1 # 查询超时时间
      max-rows: 500 # 最大行数
      fetch-size: -1 # 批处理数量
```

## 内置数据库
- SpringBoot提供了3种内嵌数据库供开发者选择，提供开发测试效率
  - H2
  - HSQL
  - Derby
### 内嵌数据库(H2)
- 导入H2相关坐标
```xml
<dependencies>
  <dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
  </dependency>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
  </dependency>
</dependencies>
```
- 设置当前项目为web工程，并配置H2管理控制台参数
```yaml
server:
  port: 80
spring:
  h2:
    console:
      enabled: true
      path: /h2
```
- 访问用户sa，默认密码123456

- 设置访问数据源
```yaml
#h2数据相关的演示
server:
  port: 80
spring:
  h2:
    console:
      enabled: true
      path: /h2
  datasource:
    url: jdbc:h2:~/test
    hikari:
      driver-class-name: org.h2.Driver
      username: sa
      password: 123456
```
- 小结
```markdown
1. H2内嵌式数据库启动方式
2. H2数据库线上运行时请务必关闭
```
- SpringBoot可以根据url地址自动之别数据库种类，在保障驱动类存在的情况下，可以省略配置
```yaml
# driver-class-name
```
### 现有数据层解决方案
![img.png](img.png)

