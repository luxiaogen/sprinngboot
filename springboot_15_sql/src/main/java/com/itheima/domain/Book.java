package com.itheima.domain;

import lombok.Data;

/**
 * @author 陆小根
 * date: 2022/05/06
 * Description:
 */
@Data
public class Book {
  private Integer id;
  private String type;
  private String name;
  private String description;


}
