## Redis
- Redis是一款key-value存储结构的内存及NoSQL数据库
  - 支持多种数据存储格式
  - 支持持久化
  - 支持集群
- Redis
  - homebrew
- Redis启动
  - 服务器启动命令
`brew services start redis`
  - 客户端启动命令
`redis-cli`
### SpringBoot 整合Redis
```xml
<!--导入redis-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```
- 配置Redis(采用默认配置)
```yaml
spring:
  redis:
    host: localhost
    port: 6379
```
- 主机：`localhost`(默认)
- 端口: 6379(默认)

- RedisTemplate提供操作各种数据存储类型的接口API
![img.png](img.png)
- 客户端：RedisTemplate
```java
@SpringBootTest
class Springboot16RedisApplicationTests {
  @Autowired
  private RedisTemplate redisTemplate;
  @Test
  void set() {
    ValueOperations ops = redisTemplate.opsForValue();
    ops.set("age", 41);
  }
  @Test
  void get() {
    ValueOperations ops = redisTemplate.opsForValue();
    Object val = ops.get("age");
    System.out.println(val);
  }
  @Test
  void hset() {
    HashOperations ops = redisTemplate.opsForHash();
    ops.put("info", "a", "aa");
  }
  @Test
  void hget() {
    HashOperations ops = redisTemplate.opsForHash();
    Object val = ops.get("info", "a");
    System.out.println(val);
  }
}
```
- 小结
```markdown
1. SpringBoot整合Redis
    - 导入redis对应的starter
    - 配置
    - 提供操作Redis接口对象RedisTemplate
        - ops*: 获取各种数据类型操作接口
```
- 客户端:RedisTemplate以对象作为key和value，内部对数据进行序列化
- 客户端:StringRedisTemplate以字符串作为key和value，与Redis客户端操作等效
- lettcus与jedis区别
  - jedis连接Redis服务器是直接模式，当多线程模式下使用jedis会存在线程安全问题，解决方案可以通过配置连接池使每个连接专用，这样整体性能就大受影响
  - lettcus基于Netty框架与Redis服务器连接，不存在线程安全问题

 
  