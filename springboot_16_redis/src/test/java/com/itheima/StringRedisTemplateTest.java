package com.itheima;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * @author 陆小根
 * date: 2022/05/10
 * Description:
 */

@SpringBootTest
public class StringRedisTemplateTest {

  @Autowired
  private StringRedisTemplate stringRedisTemplate;

  @Test
  void get() {
    ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
    String val = ops.get("name");
    System.out.println(val);
  }

}
